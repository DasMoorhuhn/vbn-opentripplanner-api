class ServerInfo:
    def __init__(self, data:dict) -> None:
        self.cpu_name = data['cpuName']
        self.n_cores = data['nCores']
        self.server_version = ServerVersion(data=data['serverVersion'])


class ServerVersion:
    def __init__(self, data:dict) -> None:
        self.version = data['version']
        self.major = data['major']
        self.minor = data['minor']
        self.incremental = data['incremental']
        self.qualifier = data['qualifier']
        self.commit = data['commit']
        self.describe = data['describe']
        self.commit_time = data['commit_time']
        self.build_time = data['build_time']
        self.UID = data['UID']
        self.shortVersionString = data['shortVersionString']
        self.longVersionString = data['longVersionString']
