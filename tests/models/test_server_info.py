from unittest import TestCase
from tests.factories.server_info_factory import ServerInfoFactory
#from factories.server_info_factory import ServerInfoFactory



def test_server_info():
    server_info = ServerInfoFactory.create()
    assert server_info.n_cores == 12345
    assert server_info.server_version.major == 12345