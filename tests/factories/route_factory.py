from src.opentripplanner.models.route import Route

class RouteFactory:
  @staticmethod
  def create():
    data = {
      "routerInfo":[
        {
          "routerId":"...",
          "polygon":{
          
        },
        "buildTime":12345,
        "transitServiceStarts":12345,
        "transitServiceEnds":12345,
        "transitModes":[
          "BICYCLE",
          "SUBWAY"
        ],
        "centerLatitude":12345.0,
        "centerLongitude":12345.0,
        "hasParkRide":True,
        "travelOptions":[
          {
          "value":"...",
          "name":"..."
          },
          {
          "value":"...",
          "name":"..."
          }
        ],
        "hasBikeSharing":True,
        "hasBikePark":True,
        "lowerLeftLatitude":12345.0,
        "lowerLeftLongitude":12345.0,
        "upperRightLatitude":12345.0,
        "upperRightLongitude":12345.0
        },
        {
        "routerId":"...",
        "polygon":{
          
        },
        "buildTime":12345,
        "transitServiceStarts":12345,
        "transitServiceEnds":12345,
        "transitModes":[
          "RAIL",
          "AIRPLANE"
        ],
        "centerLatitude":12345.0,
        "centerLongitude":12345.0,
        "hasParkRide":True,
        "travelOptions":[
          {
          "value":"...",
          "name":"..."
          },
          {
          "value":"...",
          "name":"..."
          }
        ],
        "hasBikeSharing":True,
        "hasBikePark":True,
        "lowerLeftLatitude":12345.0,
        "lowerLeftLongitude":12345.0,
        "upperRightLatitude":12345.0,
        "upperRightLongitude":12345.0
        }
      ]
      }
    
    return Route(data=data["routerInfo"][0])