from src.opentripplanner.models.server_info import ServerInfo

class ServerInfoFactory:
  @staticmethod
  def create(major=12345):
      data = {
        "serverInfo" : {
          "serverInfo" : { },
          "serverVersion" : {
            "version" : "...",
            "major" : major,
            "minor" : 12345,
            "incremental" : 12345,
            "qualifier" : "...",
            "commit" : "...",
            "describe" : "...",
            "commit_time" : "...",
            "build_time" : "...",
            "UID" : 12345,
            "shortVersionString" : "...",
            "longVersionString" : "..."
          },
          "cpuName" : "...",
          "nCores" : 12345
        },
        "serverVersion" : {
          "version" : "...",
          "major" : major,
          "minor" : 12345,
          "incremental" : 12345,
          "qualifier" : "...",
          "commit" : "...",
          "describe" : "...",
          "commit_time" : "...",
          "build_time" : "...",
          "UID" : 12345,
          "shortVersionString" : "...",
          "longVersionString" : "..."
        },
        "cpuName" : "...",
        "nCores" : 12345
      }

      return ServerInfo(data=data)
